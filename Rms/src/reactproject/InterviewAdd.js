import React from'react'

import axios from 'axios'
axios.defaults.headers.get['Accepts'] = 'application/json';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
 class InterviewAdd extends React.Component
 {
     state={
         adhaarId:'',
         modeOfInterview:'',
         location:'',
         interviewPannel:'',
         refernceBy:'',
         dateOfInterview:''

     }

     handleChange = event => {
        // console.log(this.props.upcand)
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

        let interuser = {
            adhaarId: this.state.adhaarId,
            modeOfInterview: this.state.modeOfInterview,
             location: this.state.location,
            interviewPannel: this.state.interviewPannel,
            referenceBy: this.state.referenceBy,
           
            dateOfInterview: this.state.dateOfInterview,
            
            
            
        }
        axios.post('http://localhost:8080/interview/add',interuser)

            .then(res => {
                console.log(res);
                console.log(res.data);
            })
            alert("added successfully")
            this.props.history.push("/ViewInterview")

            
    }

     render() {
        return (
            <div class="mainform" >
                
                <form onSubmit={this.handleSubmit}>
                    <label>
                        CandidateId:
                <input type="text" name="adhaarId" onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        modeOfInterview:
                        <select name="modeOfInterview"  onChange={this.handleChange}>
                                        <option value=""> -- Mode of Interview -- </option>
                                        <option value= "Telephonic">Telephonic</option>
                                        <option value="F2F">F2F</option>
                                        <option value="Skype">Skype</option>
                                    </select>
                    </label>
                    <br></br>
                    <label>
                        interviewPannel:
                        <select name="interviewPannel"  onChange={this.handleChange}>
                                        <option value=""> -- interview Pannel -- </option>
                                        <option value= "Ravi Kuppusamy">Ravi Kuppusamy</option>
                                        <option value="Rupesh Kumar">Rupesh Kumar</option>
                                       
                                    </select>
                    </label>
                    <br></br>
                    <label>
                        referenceBy:
                <input type="text" name="referenceBy"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        dateOfInterview:
                <input type="date" name="dateOfInterview"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Location:
                <input type="text" name="location"  onChange={this.handleChange} />
                    </label>
                    <button>
                        submit
                    </button>
                    <br></br>
                   
                </form>


            </div>


        )
    }

}
 export default InterviewAdd