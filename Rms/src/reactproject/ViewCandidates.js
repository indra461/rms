import React from 'react'
import NavigationLabel from './NavigationLabel';
// import CandidateForm from './CandidateForm';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button, Row, Col } from 'reactstrap';
import axios from 'axios'
import { Link } from 'react-router-dom';
import FormSubmit from './FormSubmit';
import EditForm from './EditForm';
// import EditForm from './EditForm'


class ViewCandidates extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      UpdateCandidate: "",
      cu: false,
      adhaarId:''

    };
  }

  cu = () => {
    this.setState({
      cu: !this.state.cu
    });
  }

  editCandidate = (c) => {

    this.setState({
      UpdateCandidate:c,
      cu: !this.state.cu
    })
    console.log(this.state.UpdateCandidate)




  }
  componentDidMount() {

    axios.get('http://localhost:8080/candidate/all')
      .then(res => {
        const data1 = res.data;
        this.setState({ data: data1 });
        console.log(this.state.data)
      })
  }


  formEdit() {

    if (this.state.cu) {

      return <EditForm upcand={this.state.UpdateCandidate} />

    }
  }


  handleSearch (e) {
    this.setState({ adhaarId: e.target.value })
  }

  handleGoClick () {
    if (!this.props.github.isFetchingUser) {
      this.props.actions.fetchUser(this.state)
    }
  }



  onDeleteClicked = event => {
    event.preventDefault();
    var d = event.target.value
    console.log(event.target.value);
    axios.delete(`http://localhost:8080/candidate/` + d)
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
      this.setState({
        data: this.state.data.filter(cand=> cand.adhaarId!=d)

      });
  }



  renderData() {
    const cards = this.state.data.map(c => {
      return (
        <div class="cardsdispaly">
         
          <Card className="cardCandi">

            <CardText  >adhaarId : {c.adhaarId}</CardText>
            <CardText>First Name : {c.firstName}</CardText>
            <CardText>Last Name : {c.lastName}</CardText>
            <CardText>Mobile : {c.phoneNumber}</CardText>
            <CardText>alternativeNumber : {c.alternativeNumber}</CardText>
            <CardText>Email : {c.emailId}</CardText>
            <CardText>Total Experience : {c.totalExp}</CardText>
            <CardText>Relevant Experience : {c.relavantExp}</CardText>
            <CardText>Qualification : {c.qualification}</CardText>
            <CardText>Expected CTC : {c.expectedCtc}</CardText>
            <CardText>Current CTC : {c.currentCtc}</CardText>
            <CardText>Primary Skills : {c.skills}</CardText>
            <CardText>Date Of Birth : {c.dateOfBirth}</CardText>
            <CardText>Gender : {c.gender}</CardText>
            <Button value={c.adhaarId} onClick={this.onDeleteClicked}>DELETE</Button>
            {/* <Link to="/FormSubmit" className="navbar"> */}
            <Button onClick={() => this.editCandidate(c)} >
              EDIT
                    </Button>

            {/* </Link> */}
          </Card>
        </div>
      );
    })
    return cards;
  }

  render() {
    return (
      <>
        <div className="headCandi">
          CANDIDATE DETAILS</div>

          <form >
          <input
            type='text'
            onChange={this.handleSearch.bind(this)}
            value={this.state.adhaarId} />
          <button
            type='submit'
             onClick={this.handleGoClick.bind(this)}
            >
            Search
          </button>
        </form>
        <Row>
          <Col sm="6" className="cardList">
            {this.renderData()}
          </Col>
          <Col sm="6">
            {this.formEdit()}
          </Col>
        </Row>
      </>
    );
  }

}
// export { Card, CardImg, CardImgOverLay, CardText, CardBody, CardTitle };

export default ViewCandidates;