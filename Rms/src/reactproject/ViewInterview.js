import React from'react'
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button, Row, Col } from 'reactstrap';
import axios from 'axios'
// import IntervieAdd from './InterviewAdd'
import NavigationLabel from './NavigationLabel';
import PropTypes from 'prop-types';
// import { confirmable } from 'react-confirm';
// import Dialog from 'any-dialog-library';
// import { confirmAlert } from 'react-confirm-alert'
// import 'react-confirm-alert/src/react-confirm-alert.css'

import EditInterview from './EditInterview'

class ViewInterview extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          UpdateInterview: "",
          cu: false,
         
    
        };
      }









    
      cu = () => {
        this.setState({
          cu: !this.state.cu
        });
      }
    
      editInterview = (c) => {
    
        this.setState({
          UpdateInterview:c,
          cu: !this.state.cu
        })
        console.log(this.state.UpdateInterview)
    
    
    
    
      }
      componentDidMount() {
    
        axios.get('http://localhost:8080/interview/all')
          .then(res => {
            const data1 = res.data;
            this.setState({ data: data1 });
            console.log(this.state.data)
          })
      }
    
    
      formEdit() {
    
        if (this.state.cu) {
    
          return <EditInterview upInter={this.state.UpdateInterview} />
    
        }
      }
    
    
    
      onDeleteClicked = event => {
        event.preventDefault();
        //confirm("Press Ok to delete")
        // confirmAlert("press ok")
        var d = event.target.value
        console.log(event.target.value);
        axios.delete(`http://localhost:8080/Interview/` + d)
          .then(res => {
            console.log(res);
            console.log(res.data);
          })
          
          this.setState({
            data: this.state.data.filter(cand=> cand.adhaarId!=d)
    
          });
          
      }
    
    
    
      renderData() {
        const cards = this.state.data.map(c => {
          return (
            <div class="interviewshow">
              <Card className="interviewdisplay">
    
                <CardText  >AdhaarId : {c.adhaarId}</CardText>
                <CardText>ModeOfInterview : {c.modeOfInterview}</CardText>
                <CardText>Location : {c.location}</CardText> 
                <CardText>InterviewPannel : {c.interviewPannel}</CardText>
                <CardText>Reference : {c. referenceBy}</CardText>
                <CardText>DateOfInterview: {c.dateOfInterview}</CardText>
               
                
                <Button value={c.adhaarId} onClick={this.onDeleteClicked}>DELETE</Button>

                
                {/* <Link to="/FormSubmit" className="navbar"> */}
                <Button onClick={() => this.editInterview(c)} >
                  EDIT
                        </Button>
    
                {/* </Link> */}
              </Card>
            </div>
          );
        })
        return cards;
      }
    
      render() {
        return (
          <>
            <div className="headCandi">
              INTERVIEW DETAILES</div>
            <Row>
              <Col sm="6" className="cardList">
                {this.renderData()}
              </Col>
              <Col sm="6">
                {this.formEdit()}
              </Col>
            </Row>
          </>
        );
      }
    
    }

export default ViewInterview;