import React from 'react'
// import CandidateForm from './CandidateForm'
import axios from 'axios'
axios.defaults.headers.get['Accepts'] = 'application/json';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'

class FormSubmit extends React.Component {
    state = {
        adhaarId: '',
        firstName: '',
        lastName: '',
        phoneNumber: '',
        alternativeNumber: '',
        relavantExp: '',
        totalExp: '',
        emailId: '',
        skills: '',
        qualification: '',
        currentCtc: '',
        expectedCtc: '',
        dateOfBirth: '',
        gender: ''
    }

    // componentDidMount(){
    //     this.setState({
    //         adhaarId:this.props.upcand.adhaarId,
    //         firstName:this.props.upcand.firstName,
    //         lastName:this.props.upcand.lastName,
    //         phoneNumber:this.props.upcand.phoneNumber,
    //         alternativeNumber:this.props.upcand.alternativeNumber,
    //         relavantExp:this.props.upcand.relavantExp,
    //         totalExp:this.props.upcand.totalExp,
    //         emailId:this.props.upcand.emailId,
    //         skills:this.props.upcand.skills,
    //         qualification:this.props.upcand.qualification,
    //         currentCtc:this.props.upcand.currentCtc,
    //         expectedCtc:this.props.upcand.expectedCtc,
    //         dateOfBirth:this.props.upcand.dateOfBirth,
    //         gender:this.props.upcand.gender
    //     })
    // }
    handleChange = event => {
         console.log(this.props.upcand)
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

        let user = {
            adhaarId: this.state.adhaarId,
            firstName: this.state.firstName,
             lastName: this.state.lastName,
            phoneNumber: this.state.phoneNumber,
            alternativeNumber: this.state.alternativeNumber,
            relavantExp: this.state.relavantExp,
            totalExp: this.state.totalExp,
            emailId: this.state.emailId,
            skills: this.state.skills,
            qualification: this.state.qualification,
            currentCtc: this.state.currentCtc,
            expectedCtc: this.state.expectedCtc,
            dateOfBirth: this.state.dateOfBirth,
            gender: this.state.gender
            
        }
       

        
    

        axios.post('http://localhost:8080/candidate/add',user)

            .then(res => {
                console.log(res);
                console.log(res.data);
            })
            alert("added successfully")

            this.props.history.push("/ViewCandidates")
             window.location.reload();
            

            
    }
    render() {
        return (
            <div class="mainform" >
                
                <form onSubmit={this.handleSubmit}>
                    <label>
                        CandidateId:
                <input type="text" name="adhaarId" onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        FirstName:
                <input type="text" name="firstName"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        LastName:
                <input type="text" name="lastName"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Mobile:
                <input type="text" name="phoneNumber"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        alternativeNumber:
                <input type="text" name="alternativeNumber"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        emailId:
                <input type="text" name="emailId"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        TotalExp:
                <input type="text" name="totalExp"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        RelavantExp:
                <input type="text" name="relavantExp"  onChange={this.handleChange}/>
                    </label>
                    <br></br>
                    <label>
                        Qualification:
                <input type="text" name="qualification"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Expected CTC:
                <input type="text" name="expectedCtc"  onChange={this.handleChange}/>
                    </label>
                    <br></br>
                    <label>
                        Current CTC:
                <input type="text" name="currentCtc"  onChange={this.handleChange}/>
                    </label>
                    <br></br>
                    <label>
                        Skills:
                <input type="text" name="skills"  onChange={this.handleChange}/>
                    </label>
                    <br></br>
                    <label>
                        Date of birth:
                <input type="date" name="dateOfBirth"  onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Gender:
                        <select name="gender" onChange={this.handleChange}>
                                        <option value=""> -- select gender -- </option>
                                        <option value= "Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                    </label>
                    <br></br>
                    <button>
                        submit
                    </button>
                    <br></br>
                   
                </form>


            </div>


        )
    }

}
export default FormSubmit;