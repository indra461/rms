import React from 'react'
import axios from 'axios'
import ViewCandidates from './ViewCandidates'


class EditForm extends React.Component {

    state = {
        adhaarId: '',
        firstName: '',
        lastName: '',
        phoneNumber: '',
        alternativeNumber: '',
        relavantExp: '',
        totalExp: '',
        emailId: '',
        skills: '',
        qualification: '',
        currentCtc: '',
        expectedCtc: '',
        dateOfBirth: '',
        gender: '',

    }

    componentDidMount() {
        this.setState({
            adhaarId: this.props.upcand.adhaarId,
            firstName: this.props.upcand.firstName,
            lastName: this.props.upcand.lastName,
            phoneNumber: this.props.upcand.phoneNumber,
            alternativeNumber: this.props.upcand.alternativeNumber,
            relavantExp: this.props.upcand.relavantExp,
            totalExp: this.props.upcand.totalExp,
            emailId: this.props.upcand.emailId,
            skills: this.props.upcand.skills,
            qualification: this.props.upcand.qualification,
            currentCtc: this.props.upcand.currentCtc,
            expectedCtc: this.props.upcand.expectedCtc,
            dateOfBirth: this.props.upcand.dateOfBirth,
            gender: this.props.upcand.gender
        })
    }

    handleChange = (event) => {
        console.log(this.props.upcand)
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

        let userr = {
            adhaarId: this.state.adhaarId,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            phoneNumber: this.state.phoneNumber,
            alternativeNumber: this.state.alternativeNumber,
            relavantExp: this.state.relavantExp,
            totalExp: this.state.totalExp,
            emailId: this.state.emailId,
            skills: this.state.skills,
            qualification: this.state.qualification,
            currentCtc: this.state.currentCtc,
            expectedCtc: this.state.expectedCtc,
            dateOfBirth: this.state.dateOfBirth,
            gender: this.state.gender

        }





        axios.put('http://localhost:8080/candidate', userr)

            .then(res => {
                console.log(res);
                console.log(res.data);

            })
            alert("updated successfully")
        // this.props.history.push("/ViewCandidates")
        window.location.reload();

    }







    renderForm() {
        const { adhaarId, firstName, lastName, phoneNumber, alternativeNumber, relavantExp, totalExp, emailId, skills,
            qualification, currentCTC, expectedCTC, dob, dateOfBirth } = this.state
        return (
            <div class="mainform" >

                <form onSubmit={this.handleSubmit}>
                    <label>
                        CandidateId:
                <input type="text" name="adhaarId" value={this.state.adhaarId} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        FirstName:
                <input type="text" name="firstName" value={this.state.firstName} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        LastName:
                <input type="text" name="lastName" value={this.state.lastName} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Mobile:
                <input type="text" name="phoneNumber" value={this.state.phoneNumber} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        alternativeNumber:
                <input type="text" name="alternativeNumber" value={this.state.alternativeNumber} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        emailId:
                <input type="text" name="emailId" value={this.state.emailId} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        TotalExp:
                <input type="text" name="totalExp" value={this.state.totalExp} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        RelavantExp:
                <input type="text" name="relavantExp" value={this.state.relavantExp} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Qualification:
                <input type="text" name="qualification" value={this.state.qualification} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Expected CTC:
                <input type="text" name="expectedCtc" value={this.state.expectedCtc} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Current CTC:
                <input type="text" name="currentCtc" value={this.state.currentCtc} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Skills:
                <input type="text" name="skills" value={this.state.skills} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Date of birth:
                <input type="date" name="dateOfBirth" value={this.state.dateOfBirth} onChange={this.handleChange} />
                    </label>
                    <br></br>
                    <label>
                        Gender:
                        <select name="gender" value={this.state.gender} onChange={this.handleChange}>
                            <option value=""> -- select gender -- </option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </label>
                    <br></br>
                    <button>
                        submit
                    </button>
                    <br></br>

                </form>


            </div>
        )
    }

    render() {
        return (
            <div>
                {this.renderForm()}
            </div>
        )
    }
}


export default EditForm;