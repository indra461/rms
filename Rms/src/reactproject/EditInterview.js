import React from 'react'
import axios from 'axios'
class EditInterview extends React.Component{
  
        state={
            adhaarId:'',
            modeOfInterview:'',
            location:'',
            interviewPannel:'',
            refernceBy:'',
            dateOfInterview:''
   
        }

        componentDidMount(){
            this.setState({
                adhaarId:this.props.upInter.adhaarId,
                modeOfInterview:this.props.upInter.modeOfInterview,
                location:this.props.upInter.location,
                interviewPannel:this.props.upInter.interviewPannel,
                referenceBy:this.props.upInter.referenceBy,
                dateOfInterview:this.props.upInter.dateOfInterview,
               
            })
        }
        handleChange = event => {
            console.log(this.props.upcand)
            this.setState({ [event.target.name]: event.target.value });
        }

        handleSubmit = event => {
            event.preventDefault();
    
            let userr = {
                adhaarId: this.state.adhaarId,
                modeOfInterview: this.state.modeOfInterview,
                location: this.state.location,
                interviewPannel: this.state.interviewPannel,
                refernceBy: this.state.refernceBy,
                dateOfInterview: this.state.dateOfInterview,
               
                
            }

            axios.put('http://localhost:8080/interview/update',userr)

            .then(res => {
                console.log(res);
                console.log(res.data);
                alert("added successfully")
            })
            .catch(error => {
                console.log(error.res)
            });
            

        }
    
            


        renderForm() {
            const { adhaarId, modeOfInterview, location, interviewPannel, referenceBy,dateOfInterview
                 } = this.state
            return (
                <div class="mainform" >
                    
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            CandidateId:
                    <input type="text" name="adhaarId" value={this.state.adhaarId} onChange={this.handleChange} />
                        </label>
                        <br></br>
                        <label>
                            modeOfInterview:
                            <select name="modeOfInterview" value={this.state.modeOfInterview} onChange={this.handleChange}>
                                        <option value=""> -- Mode of Interview -- </option>
                                        <option value= "Telephonic">Telephonic</option>
                                        <option value="F2F">F2F</option>
                                        <option value="Skype">Skype</option>
                                    </select>
                        </label>
                        <br></br>
                        <label>
                            interviewPannel:
                            <select name="interviewPannel" value={this.state.interviewPannel} onChange={this.handleChange}>
                                        <option value=""> -- interview Pannel -- </option>
                                        <option value= "Ravi Kuppusamy">Ravi Kuppusamy</option>
                                        <option value="Rupesh Kumar">Rupesh Kumar</option>
                                       
                                    </select>
                        </label>
                        <br></br>
                        <label>
                            referenceBy:
                    <input type="text" name="referenceBy" value={this.state.referenceBy}  onChange={this.handleChange} />
                        </label>
                        <br></br>
                        <label>
                            dateOfInterview:
                    <input type="date" name="dateOfInterview" value={this.state.dateOfInterview}  onChange={this.handleChange} />
                        </label>
                        <br></br>
                        <label>
                            Location:
                    <input type="text" name="location" value={this.state.location} onChange={this.handleChange} />
                        </label>
                        <button>
                            submit
                        </button>
                        <br></br>
                       
                    </form>
    
    
                </div>
    
    
            )
        }
        render() {
            return (
                <div>
                    {this.renderForm()}
                </div>
            )
        }
        }
        
        
    
    
    

export default EditInterview;
