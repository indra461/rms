import React, { Component } from 'react';

interface IState {
  count: number;
}
class Counter extends Component<object, IState> {
  state = {
    count: 0
  };

  onMinusClicked = () => {
    this.setState({
      count: this.state.count - 1
    });
  };

  onPlusClicked = () => {
    this.setState({
      count: this.state.count + 1
    });
  };

  render() {
    return (
      <>
        <button className="btn display-2 p-3" onClick={this.onMinusClicked}>
          -
        </button>
        <span className="p-4 text-center display-2 ">{this.state.count}</span>
        <button className="btn  display-2 p-3" onClick={this.onPlusClicked}>
          +
        </button>
      </>
    );
  }
}

export default Counter;
