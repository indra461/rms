import React from 'react';
import ProjectTableHeader from './projectTableHeader';
import projectData from './projects';

interface Project {
  [key: string]: string | boolean;
  id: string;
  name: string;
  description: string;
  owner: string;
  type: string;
}
interface IState {
  data: Project[];
}

class ProjectTable extends React.Component<object, IState> {
  state: IState = {
    data: projectData
  };

  onRowDeleteClicked = (clickedProject: Project) => {
    this.setState({
      data: this.state.data.filter(project => project.id !== clickedProject.id)
    });
  };

  renderTds(project: Project) {
    var keys = Object.keys(project);
    const tds = keys.map((key: any) => <td key={key}>{project[`${key}`]}</td>);

    return [
      ...tds,
      <td key={project.id + 'del'}>
        <button
          onClick={() => this.onRowDeleteClicked(project)}
          className="btn btn-danger"
        >
          DELETE
        </button>
      </td>
    ];
  }

  onRowClicked = (clickedProject: Project) => {
    this.setState({
      data: projectData
    });
  };
  renderRows() {
    const trs = this.state.data.map(project => {
      return (
        <tr onClick={() => this.onRowClicked(project)} key={project.id}>
          {this.renderTds(project)}
        </tr>
      );
    });

    return trs;
  }
  onAddBtnClick = () => {
    let sampleProject = {
      id: Math.floor(Math.random() * 100 + 1).toString(),
      name: 'stock ticker',
      description: 'stock ticker app frontend in angular',
      owner: 'John Watson',
      type: 'scrum'
    };

    this.setState({
      data: [sampleProject, ...this.state.data]
    });
  };
  render() {
    let keys = Object.keys(this.state.data[0]);
    return (
      <div className="row">
        <div className="col-lg-10">
          <table className="table table-hover table-striped">
            <ProjectTableHeader headerNames={keys} />
            <tbody>{this.renderRows()}</tbody>
          </table>
        </div>
        <div className="col-lg-2 mt-2">
          <button className="btn btn-danger " onClick={this.onAddBtnClick}>
            ADD PROJECT
          </button>
        </div>
      </div>
    );
  }
}

export default ProjectTable;
