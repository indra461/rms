import React from 'react';
import './App.css';

import Home from './reactproject/Home'
import Candidatedetailes from './reactproject/Candidatedetailes'
import Interviewdetailes from './reactproject/Interviewdetailes'
import NavigationLabel from './reactproject/NavigationLabel'
import { BrowserRouter, Route,Switch } from 'react-router-dom';
import ViewCandidates from './reactproject/ViewCandidates'
import FormSubmit from './reactproject/FormSubmit'
import EditForm from './reactproject/EditForm'
import InterviewAdd from './reactproject/InterviewAdd';
import ViewInterview from './reactproject/ViewInterview';
import EditInterview from './reactproject/EditInterview'
import PropTypes from 'prop-types';
import { confirmAlert } from 'react-confirm-alert'
// import DeleteCandidate from './reactproject/DeleteCandidate';



const App: React.FC = () => {
  return (
    
    <BrowserRouter>
    
    {/* <DeleteCandidate/> */}
    <NavigationLabel/>
      <Switch>
             
             <Route path="/Home" component={Home} />
            <Route path="/Candidatedetailes" component={Candidatedetailes}/>
            <Route path="/Interviewdetailes" component={Interviewdetailes} />
             <Route path="/FormSubmit" component={FormSubmit} /> 
            <Route path="/ViewCandidates" component={ViewCandidates} />
            <Route path="/EditForm" component={EditForm} />
            <Route path="/InterviewAdd" component={InterviewAdd} />
            <Route path="/ViewInterview" component={ViewInterview} />
            
          </Switch>
          
        </BrowserRouter>
       

  );
};

export default App;
